# Outil Libre Projet

Ce projet consiste à travailler sur le traitement et l'analyse des données de la base Iris.
Les données utilisées sont les mesures en centimètres des caractères suivants : longueur du sépale (Sepal.Length), largeur du sépale (Sepal.Width), longueur du pétale (Petal.Length) et largeur du pétale (Petal.Width) pour trois espèces d’iris : Iris setosa, Iris versicolor et Iris virginica.
Ces caractères sont étudiés sur une population de 150 individus (données en lignes).
La dernière colonne représente la classe réelle (la catégorie d’iris correspon- dante). Elle nous servira juste pour visualiser et évaluer nos résultats, elle n’est pas à inclure dans l’analyse.

Dans un premier temps, on enlève les données manquantes, puis on represente la distribution de chaque attribut. 
Ensuite on crée l'ensemble d'entrainement, de test et de validation.
On met en place un arbre de décision sur iris_train et on essaie d'observer le graphique pour choisir l'attribut qui permettra de mieux classer les espèces.

L'attribut choisit sera la racine de l'arbre en l'occurence ici **PetalLengthCm**
Puis on écris la fonction niveau0 et on recommence avec le graphique. Si on arrive pas à classer les espèces à l'aide du graphique(ce qui est le cas pour nous), on crée une fonction nommé **separe** qui va effectuer un balayage des coupes verticales possibles et conservera la moins mauvaise. On obtient par la suite le niveau1.

On va enchainer les deux niveaux(niveau0 et niveau1_d) grace à la fonction **arbre** et on evalue les differentes mesures
En fin on crée un classificateur d'arbre de décision en utilisant l'indice Gini
